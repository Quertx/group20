﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTransition : MonoBehaviour
{
    [Tooltip("Type in scene you wish to load (case sensitive)")]
    [SerializeField] private string NewScene;

    //Load only new scene on collision with object
    void OnCollisionEnter()
    {
        SceneManager.LoadScene(NewScene, LoadSceneMode.Single);
    }
}
