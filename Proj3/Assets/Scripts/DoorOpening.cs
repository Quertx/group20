﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpening : MonoBehaviour
{
    [SerializeField] private GameObject door;
    private bool isOpened;
    private Light light;

    // Start is called before the first frame update
    void Start()
    {
        //Ensure door is closed when loaded
        isOpened = false;
        //Grab light component and set it to green
        light = GetComponent<Light>();
        light.color = Color.white;
        light.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //Open door if currently closed
        if(isOpened == false)
        {
            door.transform.position += new Vector3(0, 4, 0);
            isOpened = true;
            light.color = Color.green;
        }
        
    }
}
