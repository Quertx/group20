﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //Get game controller to control game states
    private GameController _gameController;

    [Header("Buttons")]
    [SerializeField] private GameObject mainMenuButtons;
    [SerializeField] private GameObject credits;
    
    [Tooltip("Scene that will boot up upon clicking 'Play Game'")]
    [SerializeField] private string StartingScene;

    //Begin playing the game
    public void StartGame()
    {
        SceneManager.LoadScene(StartingScene, LoadSceneMode.Single);
    }

    //Display credits
    public void Credits()
    {
        credits.SetActive(true);
        mainMenuButtons.SetActive(false);
    }

    //Return to main menu
    public void exitCredits()
    {
        credits.SetActive(false);
        mainMenuButtons.SetActive(true);
    }

    //Exit the program
    public void ExitGame()
    {
        Application.Quit();
        Debug.Log("Exit");
    }

}
