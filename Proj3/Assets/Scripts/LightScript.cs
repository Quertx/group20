﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class LightScript : MonoBehaviour
{
    private Light light;
    public AudioSource gaspTrigger;
   
    [Header("Respawn")]
    private float timeUntilRespawn;
    private float RespawnTimer; 

    [Header("Sightline")]
    [Tooltip("Range of sight in which eyeball can see player")]
    [Range(5f, 100f)] [SerializeField] private int SightRange;
    private bool detected;

    [Header("Blinking")]
    private float lastBlink;

    [Tooltip("If turned on, light will turn on and off in given intervals")]
    [SerializeField] private bool Blink;

    [Tooltip("Time before eyeball stops searching")]
    [SerializeField] private int BlinkInterval;

    [Tooltip("Time before eyeball restarts searching")]
    [SerializeField] private int BlinkDuration;

    // Start is called before the first frame update
    void Start()
    {
        //Grab light component and set it to be enabled and green when loaded
        light = GetComponent<Light>();
        light.color = Color.green;
        light.enabled = true;

        //Ensure detection isnt true to prevent the level instantly reloading
        detected = false;
        
        //Time until level reset
        RespawnTimer = 2;

        //Get audio source for gasping from player
        gaspTrigger = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Update timer that tracks when the eyeball should blink
        float elapsed = Time.deltaTime;
        lastBlink += elapsed;

        //Check if the eyeball needs to open/close
        if ((lastBlink >= BlinkInterval) && (light.enabled == true) && Blink)
        {
            light.enabled = false;
            lastBlink = 0f;
        }
        else if ((lastBlink >= BlinkDuration) && (light.enabled == false) && Blink)
        {
            light.enabled = true;
            lastBlink = 0f;
        }
    }

    void FixedUpdate()
    {
        float elapsed = Time.deltaTime;

        RaycastHit hit;
        //Check if raycast collides with player
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, SightRange) && hit.rigidbody != null)
        {
            //Player has been seen, punish player
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
            Debug.Log("Did Hit");

            //Trigger effects on player detection
            light.color = Color.red;
            gaspTrigger.Play(0);
            detected = true;

            //Prevent player from moving on detection (reloads on player after scene reset)
            Destroy(hit.collider.gameObject.GetComponent<PlayerScript>());
            

        }
        else
        {
            //Player has not been seen
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.white);
            Debug.Log("Did not Hit");
        }

        //Reset the scene if the player has been detected
        if (detected)
        {
            //Update timer that tracks when to reload the scene
            timeUntilRespawn += elapsed;

            //Reload the scene if timer exceeds respawn time
            if (timeUntilRespawn >= RespawnTimer)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }
}

