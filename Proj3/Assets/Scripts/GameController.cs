﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    //Enum holding all possible game states
    public enum EGameState
    {
        MainMenu,
        Playing,
        Paused,
        GameOver
    }
    private EGameState _eGameState;

    private static GameController _gameController = null;

    void Awake()
    {
        //Assert we don't already have a game controller
        Debug.Assert(_gameController == null, this.gameObject);
        _gameController = this;
        
        //Set gamestate on bootup
        _eGameState = EGameState.MainMenu;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Change what is updating based on gamestate
        switch (_eGameState)
        {
            //The game is at the main menu
            case EGameState.MainMenu:
                break;

            //We are playing the game
            case EGameState.Playing:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ChangeState(EGameState.Paused);
                }
                break;

            //The game is paused
            case EGameState.Paused:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ChangeState(EGameState.Playing);
                }
                break;

            default:
                break;
        }    
    }

    //Change between gamestates when called
    public void ChangeState (EGameState eGameState)
    {
        switch (eGameState)
        {
            //The game is at the main menu
            case EGameState.MainMenu:
                break;

            //We are playing the game
            case EGameState.Playing:

                //Swap from main menu to the game
                if (_eGameState == EGameState.MainMenu)
                {
                    SceneManager.UnloadSceneAsync("MainMenu");
                    LoadGame();
                }
                //Update timescale
                
                Time.timeScale = 1.0f;
                break;

            //The game is paused
            case EGameState.Paused:
                //Update timescale
                Time.timeScale = 0.0f;
                break;

            default:
                break;
        }

        //Set game state to new state
        _eGameState = eGameState;
    }
    
    //Load game when bootup
    private void LoadGame()
    {
        SceneManager.LoadScene("TestScene", LoadSceneMode.Additive);

        //Unload any duplicate scenes
        Debug.Log("Game already on - unloading");
        if (SceneManager.GetSceneByName("TestScene").isLoaded)
        {
            SceneManager.UnloadSceneAsync("TestScene");
        }
    }
}
