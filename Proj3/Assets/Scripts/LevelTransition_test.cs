﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTransition_test : MonoBehaviour
{
    [SerializeField] private string NewScene;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        // Loads scene that can be specified as a variable
        Debug.Log("Trigger");
        SceneManager.LoadScene(NewScene);

    }
}
