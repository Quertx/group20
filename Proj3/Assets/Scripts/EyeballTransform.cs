﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeballTransform : MonoBehaviour
{
    private Transform _eyeballTransform;
    private bool reverse;
    private float elapsed;

    [Header("Movement")]
    [Range(-10f, 10f)] [SerializeField] private float XAxisTranslateSpeed;
    [Range(-10f, 10f)] [SerializeField] private float YAxisTranslateSpeed;
    [Range(-10f, 10f)] [SerializeField] private float ZAxisTranslateSpeed;

    [Header("Rotation")]
    [Range(-360f, 360f)] [SerializeField] private float XAxisRotateSpeed;
    [Range(-360f, 360f)] [SerializeField] private float YAxisRotateSpeed;
    [Range(-360f, 360f)] [SerializeField] private float ZAxisRotateSpeed;

    [Header("Reverse")]
    [SerializeField] private bool ReverseTransformations;
    [Range(0f, 10f)] [SerializeField] private float TimeUntilReverse;

    // Start is called before the first frame update
    void Start()
    {
        _eyeballTransform = GetComponent<Transform>();
        reverse = false;
        elapsed = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        //Add to timer
        elapsed += Time.deltaTime;

        //Check if eyeball should reverse itself 
        if ((ReverseTransformations) && elapsed >= TimeUntilReverse)
        {
            if (reverse)
                reverse = false;
            else
                reverse = true;
            elapsed = 0f;
        }

        //Work out translation speed per second
        float translateX = XAxisTranslateSpeed * Time.deltaTime;
        float translateY = YAxisTranslateSpeed * Time.deltaTime;
        float translateZ = ZAxisTranslateSpeed * Time.deltaTime;

        //Decide which direction to translate
        if (!reverse)
            _eyeballTransform.Translate(translateX, translateY, translateZ);
        else
            _eyeballTransform.Translate(-translateX, -translateY, -translateZ);

        //Work out rotation speed per second
        float rotateX = XAxisRotateSpeed * Time.deltaTime;
        float rotateY = YAxisRotateSpeed * Time.deltaTime;
        float rotateZ = ZAxisRotateSpeed * Time.deltaTime;
        
        //Work out which direction to rotate
        if(!reverse)
            _eyeballTransform.Rotate(rotateX, rotateY, rotateZ);
        else
            _eyeballTransform.Rotate(-rotateX, -rotateY, -rotateZ);


    }
}
