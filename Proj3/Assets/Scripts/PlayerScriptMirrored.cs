﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerScriptMirrored : MonoBehaviour
{
    //Component variables
    private Transform _PlayerTransform;
    private Rigidbody _PlayerRB;

    //Movement variables
    [Header("Movement")]

    [Tooltip("Speed at which player moves (0-10, 5 is base speed)")]
    [SerializeField] [Range(0.0f, 10.0f)] private float movementSpeed;

    [Tooltip("Power of player jump (0-5, 2 is base power)")]
    [SerializeField] [Range(0.0f, 5.0f)] private float jumpPower;

    private bool canJump = true;

    // Start is called before the first frame update
    void Start()
    {
        _PlayerRB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //Amplify jump power for use in force
        float jump = jumpPower * 10000;
        jump *= Time.deltaTime;

        //Create vector containing direction player is moving
        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal"), 0, (Input.GetAxisRaw("Vertical")));
        //Apply movement to the player per second
        _PlayerRB.transform.Translate(movement * Time.deltaTime * movementSpeed);

        //Check if the player is wanting to jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Check if the player can jump
            if (canJump == true)
            {
                _PlayerRB.AddForce(transform.up * jump);
                //Stop player from jumping in air
                canJump = false;
            }
            
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Jump resets once player is on solid ground
        if (collision.gameObject.CompareTag("Ground"))
            {
                canJump = true;
            }
    }
}
